# idapython-cheatsheet
Scripts and cheatsheets for IDAPython

![IDAPython cheatsheet (en)](IDAPython_cheatsheet_web_en.png)

## Printable versions
- [IDAPython cheatsheet (English)](https://github.com/inforion/idapython-cheatsheet/releases/download/v1.0/IDAPython_cheatsheet_print_en.png)
- [IDAPython cheatsheet (Russian)](https://github.com/inforion/idapython-cheatsheet/releases/download/v1.0/IDAPython_cheatsheet_print_ru.png)

## Links

- [The Beginner's Guide to IDAPython](https://leanpub.com/IDAPython-Book)

  